## NLP Engine for Konverz

## Purpose

This image is used hosts a NLP Engine for Konverz.

## Usage

Start the container and publish port 5000 to 5000 port on the host.

```
REGISTRY=YOUR_DOCKER_HUB_USERNAME
docker build --rm -t ${REGISTRY}/docker-hello-world .
docker run -d -p 80 ${REGISTRY}/docker-hello-world
docker push ${REGISTRY}/docker-hello-world
```

*NOTE: Replace `YOUR_DOCKER_HUB_USERNAME` above with your own Docker Hub username.*
