from flask import Flask, url_for
from flask import request
from witai_demo import Witai_Wrapper

app = Flask(__name__)

@app.route('/')
def api_root():
    return 'Welcome'

@app.route('/articles')
def api_articles():
    return 'List of ' + url_for('api_articles')

@app.route('/articles/<articleid>')
def api_article(articleid):
    return 'You are reading ' + articleid

@app.route('/witai_demo')
def witai_response():
    if 'message' in request.args:
        return get_witai_response_backend(message = request.args['message'])
    else:
        return "Sorry, I didn't understand your request"

def get_witai_response_backend(message):
    witai = Witai_Wrapper()
    #message = 'What does my day look like?'
    return witai.get_witai_params(message)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
